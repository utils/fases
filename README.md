```
┌────────────────────────────────────────────────────────────────────────────┐
│                                                                            │
│   m""                                                                      │
│ mm#mm   mmm    mmm    mmm    mmm                                           │
│   #    "   #  #   "  #"  #  #   "   ___ Simple core utilities for a fully  │
│   #    m"""#   """m  #""""   """m       functional UNIX-like system.       │
│   #    "mm"#  "mmm"  "#mm"  "mmm"                                          │
│                                                                            │
└────────────────────────────────────────────────────────────────────────────┘
```

## Goal

The `fases` project tries to provide friendly, functionnal and simple core 
utilities for a fully functionnal UNIX-like Operating System. It tries to 
be entirely portable and working on any UNIX-like Operating System and kernel 
such as OpenBSD and Linux. It also tries to be completly modular and as such 
one utility should **not** depend on another in order to work. The coreutils 
are still a work-in-progress.
The `fases` utilities are currently tested on Artix, Alpine, OpenBSD, OS X and
FreeBSD. We expect all utilities to work on all systems implementing POSIX 
due to us using only POSIX-compliant functions.

See [https://utils.vitali64.duckdns.org](https://utils.vitali64.duckdns.org)

![](./wip.gif)
